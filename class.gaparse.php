<?php
/**
 * @file
 * Define PHP Google Analytics Parser Class.
 */

/**
 * GA_Parse - PHP Google Analytics Parser Class.
 */
class GaParse { 

  public $campaignSource;
  public $campaigName;
  public $campaignMedium;
  public $campaignContent;
  public $campaignTerm;

  public $firstVisit;
  public $previousVisit;
  public $currentvisitStarted;
  public $timesVisited;

  public function __construct($cookie) {
    if (isset($cookie["__utmz"])) {
      $this->utmz = $cookie["__utmz"];
    }
    if (isset($cookie["__utma"])) {
      $this->utma = $cookie["__utma"];
    }
    $this->ParseCookies();
  }

  public function parseCookies() {
    // Parse __utmz cookie.
    if (isset($this->utmz)) {
      preg_match("((\d+)\.(\d+)\.(\d+)\.(\d+)\.(.*))", $this->utmz, $matches);
      $domain_hash = $matches[1];
      $timestamp = $matches[2];
      $session_number = $matches[3];
      $campaign_numer = $matches[4];
      $campaign_data = $matches[5];
      // Parse the campaign data.
      $campaign_data = parse_str(strtr($campaign_data, "|", "&"));
    }
    $this->campaignSource = isset($utmcsr) ? $utmcsr : '';
    $this->campaigName = isset($utmccn) ? $utmccn : '';
    $this->campaignMedium = isset($utmcmd) ? $utmcmd : '';
    $this->campaignTerm = isset($utmctr) ? $utmctr : '';
    $this->campaignContent = isset($utmcct) ? $utmcct : '';

    // You should tag you campaigns manually to have a full view
    // of your adwords campaigns data.
    // The same happens with Urchin, tag manually to have your campaign
    // data parsed properly.
    if (isset($utmgclid)) {
      $this->campaignSource = "google";
      $this->campaignName = "";
      $this->campaignMedium = "cpc";
      $this->campaignContent = "";
      $this->campaignTerm = $utmctr;
    }

    // Parse the __utma Cookie.
    if (isset($this->utma)) {
      list($domain_hash, $random_id , $time_initial_visit, $time_beginning_previous_visit, $time_beginning_current_visit, $session_counter) = preg_split('[\.]', $this->utma);
    }

    $this->firstVisit = isset($time_initial_visit) ? date("m/d/Y g:i:s A", $time_initial_visit) : '';
    $this->previousVisit = isset($time_beginning_previous_visit) ? date("m/d/Y g:i:s A", $time_beginning_previous_visit) : '';
    $this->currentvisitStarted = isset($time_beginning_current_visit) ? date("m/d/Y g:i:s A", $time_beginning_current_visit) : '';
    $this->timesVisited = isset($session_counter) ? $session_counter : '';
    // End ParseCookies.
  }  
  // End GA_Parse.
}
